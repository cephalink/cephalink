import { Files, LogOut, MessageCircle } from "lucide-react";
import Link from "next/link";
import { usePathname } from "next/navigation";
import React from "react";
import { Avatar, AvatarFallback, AvatarImage } from "~/components/ui/avatar";
import { Button } from "~/components/ui/button";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "~/components/ui/popover";
import { cn } from "~/lib/utils";
import { AuthenticatedUserProps } from "~/types/auth";
import {
  ChatRooms,
  NewChatButton,
} from "~/modules/Chat/LeftSidebar/_components";

interface DashboardLayoutProps extends AuthenticatedUserProps {
  children?: React.ReactNode;
}

export function DashboardLayout({ userAuth, children }: DashboardLayoutProps) {
  const user = userAuth?.user;
  const pathname = usePathname();

  return (
    <div className="flex min-h-screen w-full flex-row">
      <nav className="relative flex min-h-screen min-w-[17rem] max-w-xs flex-col items-start overflow-x-clip border-r-[1px] border-solid border-primary-500 bg-primary-600 p-4">
        <Link href="/">
          <h1 className="text-3xl font-bold text-white hover:text-primary-200">
            Cephalink
          </h1>
        </Link>
        <hr className="mb-2 mt-4 w-full border-[0.5px] border-solid border-primary-300" />
        <Popover>
          <PopoverTrigger
            asChild
            className="fixed bottom-4 z-[10] text-white hover:text-white"
          >
            <Button
              size="sm"
              className="flex flex-row gap-2 px-2 text-white hover:bg-primary-200"
              variant="ghost"
            >
              <Avatar className="h-7 w-7">
                <AvatarImage src="" alt={user?.name + "avatar"} />
                <AvatarFallback className="bg-primary-300">
                  {user?.name
                    ?.split(" ")
                    .map((n) => n[0])
                    .join("")}
                </AvatarFallback>
              </Avatar>
              {user?.name}
            </Button>
          </PopoverTrigger>
          <PopoverContent className="ml-4 mt-2 w-[15rem] border-solid border-primary-600 bg-primary-300">
            <p className="text-sm text-white">{user?.name}</p>
            <p className="text-xs text-white">{user?.email}</p>
            <hr className="my-2 border-primary" />
            <form action="/api/auth/logout" method="POST">
              <button
                type="submit"
                className="flex w-full flex-row items-center gap-2 rounded-md py-1 text-sm text-red-600 hover:bg-primary-200"
              >
                <LogOut size={16} />
                <p>Sign Out</p>
              </button>
            </form>
          </PopoverContent>
        </Popover>
        <div className="mb-2 max-h-full w-full overflow-y-auto">
          <ul className="w-full space-y-2 text-sm text-white">
            <li>
              <Button
                asChild
                variant="ghost"
                size="sm"
                className={cn(
                  "flex h-auto w-full flex-row items-center justify-start gap-2 p-2 hover:bg-primary-300 hover:text-white",
                  pathname.startsWith("/sources")
                    ? "bg-primary-300"
                    : "bg-primary-600",
                )}
              >
                <Link href="/sources">
                  <Files size={16} />
                  <p>Sources</p>
                </Link>
              </Button>
            </li>
            <li className="flex w-full grow flex-col gap-y-2">
              <NewChatButton />
              <ChatRooms />
            </li>
          </ul>
        </div>
      </nav>
      <main className="w-full">{children}</main>
    </div>
  );
}
