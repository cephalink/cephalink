import Head from "next/head";
import Image from "next/image";

export const LoadingHero: React.FC<{ caption?: string }> = ({
  caption = "Loading...",
}) => (
  <>
    <Head>
      <title>Cephalink</title>
      <meta name="description" content="Debug like a senior engineer" />
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <main className="flex min-h-screen w-full flex-col items-center justify-center p-8">
      <div className="flex animate-pulse flex-col items-center text-center">
        <Image
          src="/favicon.ico"
          alt="Cephalink Icon"
          className="rounded-full bg-primary-300 p-2"
          width={60}
          height={60}
        />
        <p className="mt-4 text-xl ">{caption}</p>
      </div>
    </main>
  </>
);
