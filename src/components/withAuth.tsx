import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { LoadingHero } from "~/components/layouts/LoadingHero";
import { AuthenticatedUserProps } from "~/types/auth";
import { api } from "~/utils/api";

export const withAuth: <T>(
  component: React.FC<T & AuthenticatedUserProps>,
) => React.FC = (Component) => {
  // eslint-disable-next-line
  return function WithAuth(props: any) {
    const { data, isLoading } = api.user.getAuthInfo.useQuery(undefined, {
      refetchOnWindowFocus: false,
      refetchOnMount: false,
      refetchInterval: false,
    });
    const router = useRouter();

    useEffect(() => {
      if (!isLoading && !data?.isAuthenticated) {
        void router.push("/");
      }
    }, [isLoading, data?.isAuthenticated, router]);

    return isLoading ? (
      <LoadingHero />
    ) : (
      data?.isAuthenticated && <Component {...data} {...props} />
    );
  };
};
