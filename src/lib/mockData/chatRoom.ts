import type { Chat } from "~/types/Chat";
import type { ChatRoom } from "~/types/ChatRoom";
import type { Reference } from "~/types/Reference";

const USER_ID_DUMMY = "0";

export const mockChatRooms: ChatRoom[] = [
  {
    id: "0",
    lastUpdated: new Date(),
    summary: "NullPointerError on line 18, the reference 'isActive' is not found.",
    title: "Issue #1818",
    userId: "0",
  },
  {
    id: "1",
    lastUpdated: new Date(),
    summary: "NullPointerError on line 20, the reference 'isActive' is not found.",
    title: "Issue #1819",
    userId: "0",
  },
  {
    id: "2",
    lastUpdated: new Date(),
    summary: "NullPointerError on line 22, the reference 'isActive' is not found.",
    title: "Issue #1818",
    userId: "1",
  },
];

export const mockReferences: Reference[] = [
  {
    id: "0",
    chatId: "2",
    chatRoomId: "0",
    title: "Reference 1",
    href: "https://www.google.com",
    issueNumber: 1,
    repositoryName: "Repo 1",
    sentAt: new Date(),
  },
  {
    id: "1",
    chatId: "2",
    chatRoomId: "0",
    title: "Reference 2",
    href: "https://www.google.com",
    issueNumber: 2,
    repositoryName: "Repo 2",
    sentAt: new Date(),
  },
  {
    id: "2",
    chatId: "2",
    chatRoomId: "0",
    title:
      "Reference 3 aorguaornaoe o uahgouaer augaoerughaeourb oaugrhaouerghaoerugh",
    href: "https://www.google.com",
    issueNumber: 3,
    repositoryName: "Repo 3",
    sentAt: new Date(),
  },
  {
    id: "3",
    chatId: "5",
    chatRoomId: "1",
    title: "Reference 1",
    href: "https://www.google.com",
    issueNumber: 1,
    repositoryName: "Repo 1",
    sentAt: new Date(),
  },
  {
    id: "4",
    chatId: "5",
    chatRoomId: "1",
    title: "Reference 2",
    href: "https://www.google.com",
    issueNumber: 2,
    repositoryName: "Repo 2",
    sentAt: new Date(),
  },
  {
    id: "5",
    chatId: "5",
    chatRoomId: "1",
    title:
      "Reference 3 aorguaornaoe o uahgouaer augaoerughaeourb oaugrhaouerghaoerugh",
    href: "https://www.google.com",
    issueNumber: 3,
    repositoryName: "Repo 3",
    sentAt: new Date(),
  },
  {
    id: "6",
    chatId: "5",
    chatRoomId: "1",
    title: "Reference 1",
    href: "https://www.google.com",
    issueNumber: 1,
    repositoryName: "Repo 1",
    sentAt: new Date(),
  },
];


export const mockChats: Chat[] = [
  {
    chatRoomId: "0",
    userId: USER_ID_DUMMY,
    id: "1",
    isUser: true,
    message: "Hello",
    sentAt: new Date(0),
  },
  {
    chatRoomId: "0",
    userId: USER_ID_DUMMY,
    id: "2",
    isUser: false,
    message:
      "Hi arogaeor oirghaoriga ighaoeirgaoie airghaeoirghaeorigaeog aeirghaoeirghaeorgiaehogieahgrori",
    sentAt: new Date(),
  },
  {
    chatRoomId: "0",
    userId: USER_ID_DUMMY,
    id: "3",
    isUser: true,
    message: "Hello aroguah aiguaheigua iaeuhgiuae",
    sentAt: new Date(),
  },
  {
    chatRoomId: "1",
    userId: USER_ID_DUMMY,
    id: "4",
    isUser: true,
    message: "Hello",
    sentAt: new Date(0),
  },
  {
    chatRoomId: "1",
    userId: USER_ID_DUMMY,
    id: "5",
    isUser: false,
    message:
      "Hi arogaeor oirghaoriga ighaoeirgaoie airghaeoirghaeorigaeog aeirghaoeirghaeorgiaehogieahgrori",
    sentAt: new Date(),
  },
  {
    chatRoomId: "1",
    userId: USER_ID_DUMMY,
    id: "6",
    isUser: true,
    message: "Hello aroguah aiguaheigua iaeuhgiuae",
    sentAt: new Date(),
  },
  {
    chatRoomId: "1",
    userId: USER_ID_DUMMY,
    id: "7",
    isUser: false,
    message:
      "Hi arogaeor oirghaoriga ighaoeirgaoie airghaeoirghaeorigaeog aeirghaoeirghaeorgiaehogieahgrori",
    sentAt: new Date(),
  },
  {
    chatRoomId: "1",
    userId: USER_ID_DUMMY,
    id: "8",
    isUser: true,
    message: "Hello",
    sentAt: new Date(0),
  },
  {
    chatRoomId: "2",
    userId: USER_ID_DUMMY,
    id: "9",
    isUser: false,
    message:
      "Hi arogaeor oirghaoriga ighaoeirgaoie airghaeoirghaeorigaeog aeirghaoeirghaeorgiaehogieahgrori",
    sentAt: new Date(),
  },
  {
    chatRoomId: "2",
    userId: USER_ID_DUMMY,
    id: "10",
    isUser: true,
    message: "Hello aroguah aiguaheigua iaeuhgiuae",
    sentAt: new Date(),
  },
  {
    chatRoomId: "2",
    userId: USER_ID_DUMMY,
    id: "11",
    isUser: false,
    message:
      "Hi arogaeor oirghaoriga ighaoeirgaoie airghaeoirghaeorigaeog aeirghaoeirghaeorgiaehogieahgrori",
    sentAt: new Date(),
  },
  {
    chatRoomId: "2",
    userId: USER_ID_DUMMY,
    id: "12",
    isUser: true,
    message: "Hello",
    sentAt: new Date(0),
  },
  {
    chatRoomId: "2",
    userId: USER_ID_DUMMY,
    id: "13",
    isUser: false,
    message:
      "Hi arogaeor oirghaoriga ighaoeirgaoie airghaeoirghaeorigaeog aeirghaoeirghaeorgiaehogieahgrori",
    sentAt: new Date(),
  },
];
