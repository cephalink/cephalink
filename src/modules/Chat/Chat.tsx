import Head from "next/head";
import { ChatRoom, RightSidebar } from "~/modules/Chat";
import { withAuth } from "~/components/withAuth";
import { DashboardLayout } from "../../components/layouts/DashboardLayout";

export const Chat = withAuth(({ userAuth, isAuthenticated }) => {
  return (
    <>
      <Head>
        <title>Dashboard | Cephalink</title>
        <meta
          name="description"
          content="Cephalink gathers the knowledge of all your projects in one centralized knowledge base. Finding what you need is as simple as searching in your browser."
        />
      </Head>
      <DashboardLayout userAuth={userAuth} isAuthenticated={isAuthenticated}>
        <div className="flex h-[100dvh] w-full flex-row">
          <ChatRoom />
          <RightSidebar />
        </div>
      </DashboardLayout>
    </>
  );
});
