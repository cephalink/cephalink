import { useRouter } from "next/router";

import { api } from "~/utils/api";

import { z } from "zod";

import { ChatDisplay, InputBar } from "./_components";

const chatFormSchema = z.object({
  text: z.string().min(1, "Text is too short").max(300, "Text is too long"),
});

export function ChatRoom() {
  const { query } = useRouter();
  const chatRoomId = query.id as string;

  const chatHistoryQuery = api.chat.getChats.useQuery({ chatRoomId });
  const chats = chatHistoryQuery.data ?? [];

  console.log(`ChatRoom chats:\n ${chats}`);

  const isEmpty = chatHistoryQuery.data?.length === 0;

  return (
    // TODO change to light mode -> bg gaperlu di set krn udah di globals, tinggal components pake primary-500
    <div className="flex h-full w-full flex-col-reverse gap-y-2 p-4">
      <InputBar isEmpty={isEmpty} />
      <ChatDisplay chats={chats} isLoading={chatHistoryQuery.isLoading} />
    </div>
  );
}
