import { Fragment } from "react";

import type { Chat } from "~/types/Chat";

import { MessageBubble, ReferenceLinks } from "./_components";

export type ChatDisplayProps = {
  chats: Chat[];
  isLoading: boolean;
};

export function ChatDisplay({ chats, isLoading }: ChatDisplayProps) {
  const chatsOrderedByLastSentAscending = chats?.sort(
    (a: Chat, b: Chat) => b.sentAt.getTime() - a.sentAt.getTime(),
  );

  return (
    <div className="flex h-full w-full flex-col-reverse gap-y-2 overflow-y-auto p-2">
      {isLoading && (
        <>
          <div className='w-96 h-36 rounded-lg animate-pulse bg-primary-100' />
          <div className='w-48 h-24 rounded-lg animate-pulse bg-primary-100 self-end' />
          <div className='w-96 h-60 rounded-lg animate-pulse bg-primary-100' />
          <div className='w-48 h-24 rounded-lg animate-pulse bg-primary-100 self-end flex flex-row' />
        </>
      )}
      {chatsOrderedByLastSentAscending?.map(
        ({ id, message, isUser }) => (
          <Fragment key={id}>
            <ReferenceLinks chatId={id}/>
            <MessageBubble key={id} message={message} isUser={isUser} />
          </Fragment>
          )
        )
      }
    </div>
  );
}
