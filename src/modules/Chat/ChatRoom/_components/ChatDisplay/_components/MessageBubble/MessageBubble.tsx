export type MessageBubbleProps = {
  message: string;
  isUser: boolean;
};

export function MessageBubble({ isUser, message }: MessageBubbleProps) {
  return (
    <div
      className={`flex w-full flex-row ${isUser ? "justify-end" : "justify-start"}`}
    >
      <div
        className={`max-w-[80%] rounded-lg p-2 ${isUser ? "bg-secondary-300" : "bg-primary-300"}`}
      >
        {message}
      </div>
      <p></p>
    </div>
  );
}
