import { useRouter } from "next/router";
import { useState } from "react";

import { api } from "~/utils/api";

import { GetMoreReferencesButton, ReferenceLink } from "./_components";

export type ReferenceLinksProps = {
  chatId: string;
};

export function ReferenceLinks({ chatId }: ReferenceLinksProps) {
  const [isMoreReferencesAvailable, setIsMoreReferencesAvailable] =
    useState(true);
  const { query } = useRouter();
  const chatRoomId = query.id as string;

  const referencesQuery = api.reference.getChatReferences.useQuery({ chatId });
  const references = referencesQuery.data ?? [];

  const referencesMutation = api.reference.retrieveMoreReferences.useMutation({
    onSuccess: (data) => {
      utils.reference.getChatReferences
        .invalidate({ chatId })
        .catch(console.error);
      utils.reference.getChatRoomReferences
        .invalidate({ chatRoomId })
        .catch(console.error);

      setIsMoreReferencesAvailable(data.length > 0);
    },
  });

  const utils = api.useUtils();

  async function handleGetMoreReferences() {
    referencesMutation.mutate({ chatId, chatRoomId });
  }

  return (
    <div className="reference-links flex w-4/6 flex-col items-start gap-y-1">
      {references?.map((reference, index) => (
        <ReferenceLink key={index} {...reference} />
      ))}
      {references != null &&
        references.length > 0 &&
        isMoreReferencesAvailable && (
          <GetMoreReferencesButton
            onGetMoreReferences={handleGetMoreReferences}
          />
        )}
    </div>
  );
}
