import { Button } from "~/components/ui/button";

export type GetMoreReferencesButtonProps = {
  onGetMoreReferences: () => void;
  referencesCount?: number;
};

export function GetMoreReferencesButton({
  onGetMoreReferences,
  referencesCount,
}: GetMoreReferencesButtonProps) {
  return (
    <Button
      className={`
          text-sm
          text-primary-600
          underline
        `}
      // text-primary-600 hover:text-primary-600
      onClick={onGetMoreReferences}
      variant="ghost"
    >
      Suggest {referencesCount ?? 3} more references
    </Button>
  );
}
