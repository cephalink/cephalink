import Link from "next/link";
import { CiLink } from "react-icons/ci";

import { Button } from "~/components/ui/button";
import type { Reference } from "~/types/Reference";

import { IoSparklesSharp } from "react-icons/io5";

export type ReferenceLinkProps = Reference;

export function ReferenceLink({
  href,
  id,
  issueNumber,
  repositoryName,
  title,
}: ReferenceLinkProps) {
  function handleSummarize() {
    console.log("handleReferenceLinkClick", id);
  }

  return (
    <div className="flex w-full flex-row items-center gap-x-2 truncate">
      <Link href={href} className="flex max-w-full flex-1 truncate rounded">
        <Button
          variant="outline"
          className="flex max-w-full flex-1 flex-row items-center justify-start gap-x-2 overflow-hidden truncate rounded-xl border bg-primary-300 p-2 text-sm opacity-85"
        >
          <CiLink className="shrink-0 text-lg" />
          <div className="flex flex-col items-start truncate">
            <p className="truncate text-xs text-neutral-800">
              {repositoryName} #{issueNumber}:{" "}
            </p>
            <p className="truncate">{title}</p>
          </div>
        </Button>
      </Link>
      <Button
        variant="outline"
        onClick={handleSummarize}
        className="h-full rounded-lg px-2 text-sm text-black hover:bg-primary-500 hover:text-white hover:opacity-85"
      >
        <IoSparklesSharp className="pr-[2px] hover:text-white md:pr-[4px]" />
        Summarize
      </Button>
    </div>
  );
}
