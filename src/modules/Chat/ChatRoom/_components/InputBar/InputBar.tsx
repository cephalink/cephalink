import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useRouter } from 'next/router'
import { Chat } from "openai/resources/index.mjs";
import { useForm } from "react-hook-form";
import { IoMdSend } from "react-icons/io";
import * as StreamPromises from "stream/promises";

import { Button } from "~/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from "~/components/ui/form";
import { Input } from "~/components/ui/input";
import { api } from "~/utils/api";

const formSchema = z.object({
  message: z.string()
    .min(
      1,
      "Text is too short"
    )
    .max(
      500,
      "Text is too long"
    ),
});

export type InputBarProps = {
  isEmpty: boolean;
};

// TODO JERE - this is where the chat msg gets sent. 
export function InputBar({ isEmpty }: InputBarProps) {
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
  });

  const { query } = useRouter();
  const chatRoomId = query.id as string;

  const utils = api.useUtils();

  const setTitleAndSummaryMutation = api.chatRoom.setTitleAndSummary.useMutation({
    onSuccess: () => {
      utils.chatRoom.getChatRooms.invalidate()
        .catch(console.error);
    }
  });

  const lastUpdatedMutation = api.chatRoom.setLastUpdate.useMutation({
    onSuccess: () => {
      utils.chatRoom.getChatRooms.invalidate()
        .catch(console.error);
    }
  });

  const { 
    mutate: chatMutation,
    isLoading: chatMutationIsLoading,
  } = api.chat.sendChat.useMutation({
    onSuccess: async (chatResponse) => {
      // const llmResponse = await StreamPromises.pipeline(llmResponse, );
      const responseContent = chatResponse.message;
      form.reset({ message: ""});
      utils.chat.getChats.invalidate({ chatRoomId })
        .catch(console.error);
      lastUpdatedMutation.mutate({ id: chatRoomId, lastUpdated: chatResponse.sentAt! });
      
      if (isEmpty) {
        setTitleAndSummaryMutation.mutate({
          id: chatRoomId,
        });
      }
    }
  });

  function onSubmit(values: z.infer<typeof formSchema>) {
    chatMutation({ chatRoomId, message: values.message, role: "user" });
  }

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        className="flex flex-row gap-x-2"
      >
        <FormField
          control={form.control}
          name="message"
          render={({ field }) => (
            <FormItem className="flex-1">
              <FormControl>
                <Input disabled={chatMutationIsLoading} placeholder="Ask anything about the code" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Button type="submit" className="border border-white hover:bg-gray-700">
          <IoMdSend />
        </Button>
      </form>
    </Form>
  );
}
