import type { ChatRoom } from "~/types/ChatRoom";
import { api } from "~/utils/api";

import { AccountInfo, PreviousChat, NewChatButton } from "./_components";

export type PreviousChat = Pick<
  ChatRoom, 
  "id" | "lastUpdated" | "summary" | "title"
>

export type LeftSidebarProps = {
  accountEmail: string;
  accountId: string;
  accountName: string;
  accountProfilePicture: string;
  previousChats: PreviousChat[];
};

export function LeftSidebar({
  accountEmail,
  accountId,
  accountName,
  accountProfilePicture,
}: LeftSidebarProps) {

  const chatRoomsQuery = api.chatRoom.getChatRooms.useQuery();

  const previousChats = chatRoomsQuery.data ?? [];

  const orderedPreviousChatsByLastUpdated = previousChats.sort(
    (a, b) => b.lastUpdated.getTime() - a.lastUpdated.getTime(),
  );

  return (
    <div className="flex w-72 h-screen flex-col justify-between bg-gray-900 bg-opacity-90 text-white p-4">
      <div className="flex grow h-full flex-col gap-y-4 overflow-y-auto">
        <h3 className="text-2xl">Cephalink</h3>
        <div className="border-[.25px] border-gray-400"></div>
        <NewChatButton />

        <div className="grow overflow-y-auto">
          <div className="flex w-full flex-col divide-y h-full">
            {chatRoomsQuery.isLoading && (
              [1, 2, 3, 4, 5].map((i) => (
                <div key={i} className='w-68 h-12 rounded-lg animate-pulse bg-primary-100 my-1' />
              ))
            )}
            {orderedPreviousChatsByLastUpdated.map(({ id, summary, title }) => (
              <PreviousChat key={id} id={id} summary={summary} title={title} />
            ))}
          </div>
        </div>

      </div>
      <div className="flex-none">
        <AccountInfo
          email={accountEmail}
          userId={accountId}
          name={accountName}
          profilePicture={accountProfilePicture}
        />
      </div>
    </div>
  );
}
