import { Button } from "~/components/ui/button";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "~/components/ui/dropdown-menu";

import { LogoutButton, RoundedPicture } from "./_components";

export type AccountInfoProps = {
  email: string;
  name: string;
  userId: string;
  profilePicture: string;
};

export function AccountInfo({
  email,
  name,
  userId,
  profilePicture,
}: AccountInfoProps) {
  return (
    <DropdownMenu>
      <DropdownMenuTrigger className="w-full" asChild>
        <Button
          variant="ghost"
          className="relative flex h-fit w-full flex-row items-center gap-x-4 p-3 hover:bg-neutral-200"
        >
          <RoundedPicture
            size={50}
            src={profilePicture}
            alt="Profile Picture"
          />
          <div className="flex w-full flex-col items-start gap-y-1 truncate">
            <p className="text-lg">{name}</p>
            <p className="text-sm">{email}</p>
          </div>
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent align="start">
        <DropdownMenuLabel>Account</DropdownMenuLabel>
        <DropdownMenuSeparator />
        <LogoutButton userId={userId} />
      </DropdownMenuContent>
    </DropdownMenu>
  );
}
