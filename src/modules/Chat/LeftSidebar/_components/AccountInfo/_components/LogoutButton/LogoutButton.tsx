import { Button } from "~/components/ui/button";

import { RiLogoutCircleRLine } from "react-icons/ri";

export type LogoutButtonProps = {
  userId: string;
};

export function LogoutButton({ userId }: LogoutButtonProps) {
  function handleLogout() {
    console.log("Logout", userId);
  }
  return (
    <Button
      variant="ghost"
      className="flex w-full flex-row items-center justify-start gap-x-1 text-red-600 hover:text-red-600"
      onClick={handleLogout}
    >
      <RiLogoutCircleRLine className="text-xl" />
      <p>Logout</p>
    </Button>
  );
}
