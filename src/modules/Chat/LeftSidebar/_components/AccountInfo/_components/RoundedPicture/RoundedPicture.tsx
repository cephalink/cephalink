import Image from "next/image";

export type RoundedPictureProps = {
  alt: string;
  size: number;
  src: string;
};

export function RoundedPicture({ alt, size, src }: RoundedPictureProps) {
  return (
    <div
      className={`relative shrink-0 rounded-full h-[${size}px] w-[${size}px] overflow-hidden`}
    >
      <Image
        src={src}
        alt={alt}
        fill
        style={{
          objectFit: "cover",
        }}
      />
    </div>
  );
}
