import { api } from "~/utils/api";

import { PreviousChat } from "../PreviousChat";

export function ChatRooms() {
  const chatRoomsQuery = api.chatRoom.getChatRooms.useQuery();

  const ChatRooms = chatRoomsQuery.data ?? [];

  const orderedChatRoomsByLastUpdated = ChatRooms.sort(
    (a, b) => b.lastUpdated.getTime() - a.lastUpdated.getTime(),
  );

  return (
    <div className="flex h-full grow flex-col gap-y-4 overflow-y-auto">
      <div className="grow overflow-y-auto">
        <ul className="flex h-full w-full flex-col divide-y">
          {chatRoomsQuery.isLoading &&
            [1, 2, 3, 4, 5].map((i) => (
              <li
                key={i}
                className="w-68 my-1 h-12 animate-pulse rounded-lg bg-primary-100"
              />
            ))}
          {orderedChatRoomsByLastUpdated.map(({ id, summary, title }) => (
            <PreviousChat key={id} id={id} summary={summary} title={title} />
          ))}
        </ul>
      </div>
    </div>
  );
}
