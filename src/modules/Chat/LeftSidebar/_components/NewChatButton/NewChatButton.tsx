import { useRouter } from "next/router";
import { HiMiniPencilSquare } from "react-icons/hi2";

import { Button } from "~/components/ui/button";
import { api } from "~/utils/api";

export function NewChatButton() {
  const router = useRouter();

  const utils = api.useUtils();
  const addNewChatMutation = api.chatRoom.addNewChat.useMutation({
    onSuccess: (chatRoom) => {
      utils.chatRoom.getChatRooms.invalidate().catch(console.error);

      router.push(`/chat/${chatRoom.id}`).catch(console.error);
    },
  });

  function handleClick() {
    addNewChatMutation.mutate();
  }

  return (
    <Button
      variant="ghost"
      onClick={handleClick}
      className="p-200 flex w-full grow flex-row items-center justify-start gap-x-1 rounded-lg border border-gray-400 border-opacity-50 p-2 hover:bg-neutral-200"
    >
      <HiMiniPencilSquare className="text-xl" />
      <span>New Chat</span>
    </Button>
  );
}
