import { useRouter } from 'next/router'
import Link from "next/link";
import { Button } from "~/components/ui/button";

export type PreviousChatProps = {
  id: string;
  summary?: string;
  title?: string;
};

export function PreviousChat({ id, summary = "Chat here", title = "New Chat" }: PreviousChatProps) {
  const { query } = useRouter();
  const chatRoomId = query.id as string;
  const isActive = chatRoomId === id;
  console.log(isActive)

  return (
    <div className="rounded-none">
      <Link href={`/chat/${id}`}>
        <Button
          variant="ghost"
          className={`group flex h-fit w-full flex-col justify-start items-start p-2 group-hover:bg-opacity-50 ${isActive && "bg-neutral-100 text-neutral-800"}`}
        >
          <p title={title} className={`text-start w-full truncate text-sm group-hover:font-bold ${isActive && "font-bold"}`}>{title}</p>
          <p title={summary} className="text-start w-full truncate">{summary}</p>
        </Button>
      </Link>
    </div>
  );
}
