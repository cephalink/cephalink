import { useRouter } from "next/router";

import { ReferenceLink } from "../ChatRoom/_components";
import { api } from "~/utils/api";

export function RightSidebar() {
  const { query } = useRouter();
  const chatRoomId = query.id as string;

  const referencesQuery = api.reference.getChatRoomReferences.useQuery({
    chatRoomId,
  });
  const references = referencesQuery.data ?? [];

  const isReferencesEmpty = references.length === 0;

  const orderedreferencesBySentAt = references.sort(
    (a, b) => b.sentAt.getTime() - a.sentAt.getTime(),
  );

  return (
    <div className="flex h-full w-80 flex-col justify-start gap-y-2 bg-primary-600 bg-opacity-90 py-4">
      <div className="px-4">
        <h3 className="text-lg text-white">Issues we&apos;ve discussed</h3>
        {!referencesQuery.isLoading && isReferencesEmpty && (
          <p className="text-sm text-white">
            Start asking and references will pop up here
          </p>
        )}
      </div>

      <div className="w-80 overflow-y-auto">
        <div className="flex w-80 shrink-0 flex-col gap-y-2 overflow-y-auto px-4">
          {referencesQuery.isLoading &&
            [1, 2, 3, 4, 5].map((i) => (
              <div
                key={i}
                className="h-8 w-72 animate-pulse rounded-xl bg-primary-100"
              />
            ))}
          {orderedreferencesBySentAt.map((reference) => (
            <ReferenceLink key={reference.id} {...reference} />
          ))}
        </div>
      </div>
    </div>
  );
}
