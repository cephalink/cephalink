import Head from "next/head";
import { LandingPage } from "./LandingPage";
import { api } from "~/utils/api";
import { useRouter } from "next/router";
import { useEffect } from "react";

export function HomePage() {
  const { data, isLoading } = api.user.getAuthInfo.useQuery();
  const router = useRouter();

  useEffect(() => {
    if (!isLoading && data?.isAuthenticated) {
      void router.push("/sources");
    }
  }, [router, isLoading, data]);

  return (
    <>
      <Head>
        <title>Cephalink</title>
        <meta
          name="description"
          content="Cephalink gathers the knowledge of all your projects in one centralized knowledge base. Finding what you need is as simple as searching in your browser."
        />
      </Head>
      {isLoading ? null : !data?.isAuthenticated && <LandingPage />}
    </>
  );
}
