import { Icon } from "@iconify/react";
import Link from "next/link";
import { Button } from "../../components/ui/button";

export function LandingPage() {
  return (
    <main className="flex min-h-screen w-full flex-col items-center justify-center p-8">
      <div className="flex flex-col items-center text-center">
        <h1 className="text-5xl font-bold">
          Your entire team's knowledge <br className="my-2" />
          <span className="text-bold text-primary">within a single prompt</span>
        </h1>

        <p className="my-6 max-w-xl text-lg md:max-w-2xl md:text-xl">
          Cephalink gathers the knowledge of all your projects in one{" "}
          <strong className="text-secondary">
            centralized knowledge base.
          </strong>{" "}
          Finding what you need is{" "}
          <strong className="text-primary">
            as simple as searching in your browser.
          </strong>
        </p>

        <Button
          asChild
          className="bg-primary px-10 py-7 text-base hover:bg-primary-600"
        >
          <Link
            href="/api/auth/login/gitlab"
            className="flex flex-row items-center gap-3"
          >
            <Icon icon="devicon:gitlab" fontSize={24} />
            <p className="pb-1">Sign in with Gitlab</p>
          </Link>
        </Button>
      </div>
    </main>
  );
}
