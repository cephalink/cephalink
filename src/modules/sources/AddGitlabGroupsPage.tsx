import { DashboardLayout } from "../../components/layouts/DashboardLayout";
import { api } from "~/utils/api";
import { LoadingHero } from "~/components/layouts/LoadingHero";
import { withAuth } from "~/components/withAuth";
import Head from "next/head";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardTitle,
} from "~/components/ui/card";
import { Checkbox } from "~/components/ui/checkbox";
import { useFieldArray, useForm } from "react-hook-form";
import { Form } from "~/components/ui/form";
import { Label } from "~/components/ui/label";
import { Button } from "~/components/ui/button";
import Link from "next/link";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { addGitlabGroupsFormSchema } from "~/types/resource/forms";
import { useToast } from "~/components/ui/use-toast";
import { useRouter } from "next/router";

export const AddGitlabGroupsPage = withAuth(({ userAuth, isAuthenticated }) => {
  const { toast } = useToast();
  const router = useRouter();

  const { data: addedGroups, isLoading: addedGroupsLoading } =
    api.resource.getGroups.useQuery(undefined, {
      refetchInterval: false,
      refetchOnWindowFocus: false,
    });

  const { data, isLoading: getGroupsLoading } =
    api.resource.gitlab.getGroups.useQuery(undefined, {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    });

  const {
    mutateAsync: addGroupsToSystem,
    isLoading: addGroupsToSystemLoading,
    isError: addGroupsToSystemError,
  } = api.resource.gitlab.addGroupsToSystem.useMutation();

  const addGroupsForm = useForm<z.infer<typeof addGitlabGroupsFormSchema>>({
    resolver: zodResolver(addGitlabGroupsFormSchema),
    defaultValues: { groups: addedGroups ?? [] },
  });

  const { append, remove } = useFieldArray({
    control: addGroupsForm.control,
    name: "groups",
  });

  const addGroups = async (
    values: z.infer<typeof addGitlabGroupsFormSchema>,
  ) => {
    await addGroupsToSystem(values);

    if (!addGroupsToSystemLoading) {
      if (addGroupsToSystemError) {
        toast({
          title: "Failed to add groups to system",
          variant: "destructive",
        });
      } else {
        toast({
          title: `Successfully added groups to system.`,
        });

        router.push("/sources");
      }
    }
  };

  return (
    <>
      <Head>
        <title>Add Groups | Cephalink</title>
        <meta
          name="description"
          content="Add your Gitlab groups to Cephalink"
        />
      </Head>
      <DashboardLayout userAuth={userAuth} isAuthenticated={isAuthenticated}>
        {getGroupsLoading || addedGroupsLoading ? (
          <LoadingHero />
        ) : (
          <div className="flex min-h-screen flex-col items-center justify-center p-6">
            <Form {...addGroupsForm}>
              <form onSubmit={addGroupsForm.handleSubmit(addGroups)}>
                <Card className="w-fit p-4 text-left">
                  <CardTitle>Add groups to Cephalink</CardTitle>
                  <CardDescription className="mt-2">
                    Select the groups you want to include in Cephalink
                  </CardDescription>
                  <CardContent className="p-0 pt-4">
                    <ul className="space-y-3">
                      {data?.map((group) => (
                        <li
                          className="flex flex-row items-center gap-2"
                          key={group.id}
                        >
                          <Checkbox
                            id={group.id.toString()}
                            className="border-primary-600 data-[state=checked]:bg-primary"
                            value={group.id}
                            defaultChecked={
                              (addedGroups ?? []).findIndex(
                                (addedGroup) =>
                                  addedGroup.id === group.id.toString(),
                              ) !== -1
                            }
                            onCheckedChange={(checked) => {
                              if (checked) {
                                append({
                                  id: group.id.toString(),
                                  name: group.name,
                                });
                              } else {
                                const groupIdx = addGroupsForm
                                  .getValues("groups")
                                  .findIndex(
                                    (addedGroup) =>
                                      addedGroup.id === group.id.toString(),
                                  );
                                remove(groupIdx);
                              }
                            }}
                          />
                          <Label htmlFor={group.id.toString()}>
                            {group.name}
                          </Label>
                        </li>
                      ))}
                    </ul>
                  </CardContent>
                  <CardFooter className="mt-4 flex justify-end gap-2 p-0">
                    <Button variant="ghost" asChild>
                      <Link href="/sources">Cancel</Link>
                    </Button>
                    <Button
                      disabled={addGroupsToSystemLoading}
                      type="submit"
                      className="bg-primary hover:bg-primary-600"
                    >
                      Confirm
                    </Button>
                  </CardFooter>
                </Card>
              </form>
            </Form>
          </div>
        )}
      </DashboardLayout>
    </>
  );
});
