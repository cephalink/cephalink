import { zodResolver } from "@hookform/resolvers/zod";
import Head from "next/head";
import Link from "next/link";
import { useParams } from "next/navigation";
import { useRouter } from "next/router";
import { useFieldArray, useForm } from "react-hook-form";
import { z } from "zod";
import { DashboardLayout } from "~/components/layouts/DashboardLayout";
import { LoadingHero } from "~/components/layouts/LoadingHero";
import { Button } from "~/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardTitle,
} from "~/components/ui/card";
import { Checkbox } from "~/components/ui/checkbox";
import { Form } from "~/components/ui/form";
import { Label } from "~/components/ui/label";
import { useToast } from "~/components/ui/use-toast";
import { withAuth } from "~/components/withAuth";
import { addGitlabProjectsFormSchema } from "~/types/resource/forms";
import { api } from "~/utils/api";

export const AddGitlabProjectsPage = withAuth(
  ({ userAuth, isAuthenticated }) => {
    const { groupId } = useParams<{ groupId: string }>();
    const { toast } = useToast();
    const router = useRouter();

    const { data, isLoading } = api.resource.gitlab.getGroupProjects.useQuery(
      { groupId },
      { refetchOnWindowFocus: false, refetchInterval: false },
    );

    const { data: addedGroupProjects, isLoading: addedGroupProjectsLoading } =
      api.resource.getGroupProjects.useQuery(
        {
          groupId,
        },
        {
          refetchInterval: false,
          refetchOnWindowFocus: false,
        },
      );

    const {
      mutateAsync: addProjectsToSystem,
      isLoading: addProjectsToSystemLoading,
      isError: addProjectsToSystemError,
    } = api.resource.gitlab.addProjectsToSystem.useMutation();

    const addProjectsForm = useForm<
      z.infer<typeof addGitlabProjectsFormSchema>
    >({
      resolver: zodResolver(addGitlabProjectsFormSchema),
      defaultValues: {
        projects: addedGroupProjects?.projects ?? [],
      },
    });

    const { append, remove } = useFieldArray({
      control: addProjectsForm.control,
      name: "projects",
    });

    const addProjects = async (
      values: z.infer<typeof addGitlabProjectsFormSchema>,
    ) => {
      await addProjectsToSystem({ groupId, projects: values.projects });
      if (!addProjectsToSystemLoading) {
        if (addProjectsToSystemError) {
          toast({
            title: "Failed to add projects to system",
            variant: "destructive",
          });
        } else {
          toast({
            title: `Successfully added projects to system.`,
          });
          router.push("/sources/groups/" + groupId);
        }
      }
    };

    return (
      <>
        <Head>
          <title>Add Projects | Cephalink</title>
        </Head>
        <DashboardLayout userAuth={userAuth} isAuthenticated={isAuthenticated}>
          {isLoading ? (
            <LoadingHero />
          ) : (
            <div className="flex min-h-screen w-full flex-col items-center justify-center p-6">
              <Form {...addProjectsForm}>
                <form onSubmit={addProjectsForm.handleSubmit(addProjects)}>
                  <Card className="w-fit p-4 text-left">
                    <CardTitle>Add projects to Cephalink</CardTitle>
                    <CardDescription className="mt-2">
                      Select the projects you want to include in Cephalink
                    </CardDescription>
                    <CardContent className="p-0 pt-4">
                      <ul className="space-y-3">
                        {data?.map((project) => (
                          <li
                            className="flex flex-row items-center gap-2"
                            key={project.id}
                          >
                            <Checkbox
                              id={project.id.toString()}
                              className="border-primary-600 data-[state=checked]:bg-primary"
                              value={project.id}
                              defaultChecked={
                                (addedGroupProjects?.projects ?? []).findIndex(
                                  (addedProject) =>
                                    addedProject.id === project.id.toString(),
                                ) !== -1
                              }
                              onCheckedChange={(checked) => {
                                if (checked) {
                                  append({
                                    id: project.id.toString(),
                                    name: project.name,
                                  });
                                } else {
                                  const projectIdx = addProjectsForm
                                    .getValues("projects")
                                    .findIndex(
                                      (addedProject) =>
                                        addedProject.id ===
                                        project.id.toString(),
                                    );
                                  remove(projectIdx);
                                }
                              }}
                            />
                            <Label htmlFor={project.id.toString()}>
                              {project.name}
                            </Label>
                          </li>
                        ))}
                      </ul>
                    </CardContent>
                    <CardFooter className="mt-4 flex justify-end gap-2 p-0">
                      <Button variant="ghost" asChild>
                        <Link href={"/sources/groups/" + groupId}>Cancel</Link>
                      </Button>
                      <Button
                        disabled={false}
                        type="submit"
                        className="bg-primary hover:bg-primary-600"
                      >
                        Confirm
                      </Button>
                    </CardFooter>
                  </Card>
                </form>
              </Form>
            </div>
          )}
        </DashboardLayout>
      </>
    );
  },
);
