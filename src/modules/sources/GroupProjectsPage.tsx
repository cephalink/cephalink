import Head from "next/head";
import Image from "next/image";
import { useParams } from "next/navigation";
import { useRouter } from "next/router";
import { DashboardLayout } from "~/components/layouts/DashboardLayout";
import { LoadingHero } from "~/components/layouts/LoadingHero";
import { Button } from "~/components/ui/button";
import { Card, CardDescription, CardTitle } from "~/components/ui/card";
import { withAuth } from "~/components/withAuth";
import { api } from "~/utils/api";

export const GroupProjectsPage = withAuth(({ userAuth, isAuthenticated }) => {
  const { groupId } = useParams<{ groupId: string }>();
  const router = useRouter();

  const { data, isLoading, isError, error } =
    api.resource.getGroupProjects.useQuery(
      { groupId },
      { refetchInterval: false },
    );

  return (
    <>
      <Head>
        <title>
          {isLoading
            ? "Loading..."
            : isError
              ? error.data?.code === "NOT_FOUND"
                ? error.message
                : "Something went wrong"
              : data.name}{" "}
          | Cephalink
        </title>
      </Head>
      <DashboardLayout userAuth={userAuth} isAuthenticated={isAuthenticated}>
        {isLoading ? (
          <LoadingHero />
        ) : (
          <div className="flex min-h-screen w-full flex-col p-6">
            <div className="flex flex-row justify-between">
              {isError ? (
                error.data?.code === "NOT_FOUND" ? (
                  <h1 className="text-4xl font-bold">{error.message}</h1>
                ) : (
                  <h1 className="text-4xl font-bold"> Something went wrong</h1>
                )
              ) : (
                <>
                  <h1 className="text-4xl font-bold">
                    Projects from {data.name}
                  </h1>
                  <Button
                    className="bg-primary"
                    onClick={() => {
                      router.push(`/sources/groups/${groupId}/add-projects`);
                    }}
                  >
                    Add Projects
                  </Button>
                </>
              )}
            </div>
            {data && data.projects.length > 0 ? (
              <div className="grid grid-cols-3 gap-6 py-6">
                {data.projects.map((project) => (
                  <Card
                    className="p-4 duration-200 hover:scale-110"
                    key={project.id}
                  >
                    <CardTitle className="mb-2 flex flex-row justify-between">
                      <p>{project.name}</p>
                    </CardTitle>
                    <CardDescription>Project ID: {project.id}</CardDescription>
                  </Card>
                ))}
              </div>
            ) : (
              <div className="flex min-h-[80vh] w-full flex-col items-center justify-center p-8">
                <div className="flex flex-col items-center text-center">
                  <Image
                    src="/favicon.ico"
                    alt="Cephalink Icon"
                    className="rounded-full bg-primary p-2"
                    width={60}
                    height={60}
                  />
                  <p className="mt-4 text-xl ">
                    No projects have been added yet...
                  </p>
                </div>
              </div>
            )}
          </div>
        )}
      </DashboardLayout>
    </>
  );
});
