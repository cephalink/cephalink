import Head from "next/head";
import { Button } from "~/components/ui/button";
import { withAuth } from "~/components/withAuth";
import { DashboardLayout } from "../../components/layouts/DashboardLayout";
import { useRouter } from "next/router";
import { api } from "~/utils/api";
import { Card, CardDescription, CardTitle } from "~/components/ui/card";
import Link from "next/link";
import Image from "next/image";

export const SourceMainPage = withAuth(({ userAuth, isAuthenticated }) => {
  const router = useRouter();

  const { data } = api.resource.getGroups.useQuery(undefined, {
    refetchOnWindowFocus: false,
    refetchInterval: false,
  });

  return (
    <>
      <Head>
        <title>Dashboard | Cephalink</title>
        <meta
          name="description"
          content="Cephalink gathers the knowledge of all your projects in one centralized knowledge base. Finding what you need is as simple as searching in your browser."
        />
      </Head>
      <DashboardLayout userAuth={userAuth} isAuthenticated={isAuthenticated}>
        <div className="flex min-h-screen w-full flex-col p-6">
          <div className="flex flex-row justify-between">
            <h1 className="text-4xl font-bold">Groups</h1>
            <Button
              className="bg-primary"
              onClick={() => {
                router.push("/sources/groups/add-group");
              }}
            >
              Add Groups
            </Button>
          </div>
          {data && data.length > 0 ? (
            <div className="grid grid-cols-3 gap-6 py-6">
              {data?.map((group) => (
                <Link href={"/sources/groups/" + group.id} key={group.id}>
                  <Card className="p-4 duration-200 hover:scale-110">
                    <CardTitle className="mb-2">{group.name}</CardTitle>
                    <CardDescription>Group ID: {group.id}</CardDescription>
                  </Card>
                </Link>
              ))}
            </div>
          ) : (
            <div className="flex min-h-[80vh] w-full flex-col items-center justify-center p-8">
              <div className="flex flex-col items-center text-center">
                <Image
                  src="/favicon.ico"
                  alt="Cephalink Icon"
                  className="rounded-full bg-primary p-2"
                  width={60}
                  height={60}
                />
                <p className="mt-4 text-xl ">
                  No groups have been added yet...
                </p>
              </div>
            </div>
          )}
        </div>
      </DashboardLayout>
    </>
  );
});
