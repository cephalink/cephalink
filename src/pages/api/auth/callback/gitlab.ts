import { User, UserAccount } from "@prisma/client";
import { OAuth2RequestError } from "arctic";
import axios from "axios";
import { Session } from "lucia";
import { NextApiRequest, NextApiResponse } from "next";
import { parseCookies } from "oslo/cookie";
import { ulid } from "~/lib/ulid";
import { AuthProviders, auth, gitlabAuth } from "~/server/auth";
import { db } from "~/server/db";

interface GitlabOIDCResponse {
  sub: string;
  name: string | null;
  email: string;
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  if (req.method !== "GET") {
    return res.status(405).json({ message: "Method not allowed" });
  }

  const cookies = parseCookies(req.headers.cookie ?? "");
  const storedState = cookies.get("gitlab_oauth_state");
  const state = req.query.state;
  const code = req.query.code;

  if (
    typeof storedState !== "string" ||
    storedState.trim() === "" ||
    typeof state !== "string" ||
    state.trim() === "" ||
    storedState !== state ||
    typeof code !== "string"
  ) {
    return res.status(400).json({ message: "Invalid auth request" });
  }

  try {
    const tokens = await gitlabAuth.validateAuthorizationCode(code);
    const response = await axios.get("https://gitlab.com/oauth/userinfo", {
      headers: {
        Authorization: `Bearer ${tokens.accessToken}`,
      },
    });
    const gitlabUser = response.data as GitlabOIDCResponse;

    let user: User;
    let account: UserAccount;

    const checkAccount = await db.userAccount.findFirst({
      where: {
        providerId: AuthProviders.GITLAB,
        providerUserId: gitlabUser.sub,
      },
    });

    if (checkAccount !== null) {
      account = checkAccount;
      user = (await db.user.findFirst({
        where: { id: account.userId },
      }))!;
    } else {
      const [newUser, newAccount] = await db.$transaction(async (tx) => {
        const user = await tx.user.create({
          data: {
            id: ulid(),
            email: gitlabUser.email,
            name: gitlabUser.name,
          },
        });

        const account = await tx.userAccount.create({
          data: {
            providerId: AuthProviders.GITLAB,
            providerUserId: gitlabUser.sub,
            userId: user.id,
          },
        });

        return [user, account];
      });

      user = newUser;
      account = newAccount;
    }

    let session: Session;

    const existingSession = await db.session.findFirst({
      where: {
        userId: user.id,
      },
    });

    if (existingSession !== null) {
      const { session: checkSession } = await auth.validateSession(
        existingSession.id,
      );
      if (checkSession !== null) {
        await auth.invalidateSession(checkSession.id);
      }
    }

    session = await auth.createSession(
      user.id,
      {
        userAccountProviderId: account.providerId,
        userAccountProviderUserId: account.providerUserId,
        accessToken: tokens.accessToken,
        refreshToken: tokens.refreshToken,
      },
      { sessionId: ulid() },
    );
    const sessionCookie = auth.createSessionCookie(session.id);

    return res
      .status(302)
      .appendHeader("Set-Cookie", sessionCookie.serialize())
      .setHeader("Location", "/")
      .end();
  } catch (error) {
    if (error instanceof OAuth2RequestError) {
      return res.status(400).json({ message: "Invalid auth request" });
    }

    console.log("Failed to process gitlab callback:", error);
    return res.status(500).json({ message: "Something went wrong" });
  }
}
