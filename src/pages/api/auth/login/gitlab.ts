import { generateState } from "arctic";
import { NextApiRequest, NextApiResponse } from "next";
import { serializeCookie } from "oslo/cookie";
import { env } from "~/env";
import { gitlabAuth } from "~/server/auth";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  if (req.method !== "GET") {
    return res.status(405).json({ message: "Method not allowed" });
  }

  const state = generateState();
  const url = await gitlabAuth.createAuthorizationURL(state, {
    scopes: ["read_api", "openid", "profile", "email"],
  });

  const stateCookie = serializeCookie("gitlab_oauth_state", state, {
    httpOnly: true,
    secure: env.NODE_ENV === "production",
    path: "/",
    maxAge: 3600,
  });

  return res
    .status(302)
    .setHeader("Set-Cookie", stateCookie)
    .setHeader("Location", url.toString())
    .end();
}
