import { NextApiRequest, NextApiResponse } from "next";
import { auth, handleAuthRequest } from "~/server/auth";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  if (req.method !== "POST") {
    return res.status(405).json({ message: "Method not allowed" });
  }

  const { session } = await handleAuthRequest(req, res);

  if (session === null) {
    return res.status(401).json({
      message: "Unauthorized",
    });
  }

  await auth.invalidateSession(session.id);

  return res.status(302).setHeader("Location", "/").end();
}
