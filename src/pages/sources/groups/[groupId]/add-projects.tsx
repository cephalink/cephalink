import { AddGitlabProjectsPage } from "~/modules/sources/AddGitlabProjectsPage";

export default function AddGitlabProjects() {
  return <AddGitlabProjectsPage />;
}
