import { SourceMainPage } from "~/modules/sources/SourceMainPage";

export default function SourceMain() {
  return <SourceMainPage />;
}
