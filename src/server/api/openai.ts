import { OpenAI } from "openai";
import { ChatOpenAI } from "langchain/chat_models/openai";
import { OpenAI as LangChainOpenAI } from "langchain/llms/openai";
import { HuggingFaceTransformersEmbeddings } from "langchain/embeddings/hf_transformers";
import { env } from "~/env";


export const MODEL_NAME = "gpt-4-0125-preview";
export const EMBEDDING_MODEL = "Xenova/bge-large-en-v1.5";
export const TEMPERATURE = 0.7;

export const openai = new OpenAI({
  organization: env.OPENAI_ORGANIZATION_ID,
  apiKey: env.OPENAI_API_KEY,
});

export const OpenAILangChain = new LangChainOpenAI({
  modelName: MODEL_NAME,
  openAIApiKey: env.OPENAI_API_KEY,
  temperature: TEMPERATURE,
});

export const ChatOpenAILangChain = new ChatOpenAI({
  modelName: MODEL_NAME,
  openAIApiKey: env.OPENAI_API_KEY,
  temperature: TEMPERATURE,
});

export const EmbeddingModel = new HuggingFaceTransformersEmbeddings({
  modelName: EMBEDDING_MODEL,
  maxConcurrency: 10,
});
