import { userRouter } from "~/server/api/routers/user";
import { createTRPCRouter } from "~/server/api/trpc";
import { chatRouter } from "./routers/chat";
import { chatRoomRouter } from "./routers/chatRoom";
import { referenceRouter } from "./routers/reference";
import { resourceRouter } from "./routers/resource";

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here.
 */
export const appRouter = createTRPCRouter({
  user: userRouter,
  chat: chatRouter,
  chatRoom: chatRoomRouter,
  reference: referenceRouter,
  resource: resourceRouter,
});

// export type definition of API
export type AppRouter = typeof appRouter;
