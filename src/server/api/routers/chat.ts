import { z } from 'zod';
import { ulid } from 'ulid';
import { Message, OpenAIStream, streamToResponse } from "ai";
import { ChatCompletionMessageParam, ChatCompletionFunctionMessageParam, ChatCompletion } from "openai/resources/index.mjs";

import { mockChats } from '~/lib/mockData';
import type { Chat, SendChatRequest } from "~/types/Chat";

import { 
  createTRPCRouter,
  publicProcedure
} from "../trpc";

import { chatToMessage } from '~/utils/chatToMessage';
import * as llm from "~/server/api/openai";


const USER_ID_DUMMY = "0";
const CHATROOM_ID = "0";

interface Prompt {
  role: "function" | "system" | "user" | "assistant" | "tool";
  content: string;
}

interface LLMResponseInput {
  prompt: Prompt;
  messages: Message[];
}

export const chatRouter = createTRPCRouter({
  getChats: publicProcedure
    .input(z.object({chatRoomId: z.string()}))
    .query((opts) => {
      const { chatRoomId } = opts.input;

      // Mock filtering chats by chatRoomId
      const chats: Chat[] = mockChats.filter((chat) => chat.chatRoomId === chatRoomId);

      return chats;
    }),
  
  // TODO JERE - when sending chats: store to db and also feed into LLM (make separate method for this)
  sendChat: publicProcedure 
    .input(z.object({
      chatRoomId: z.string(), 
      message: z.string(),
      role: z.string(),
    }))
    .mutation(async (opts) => {
      const { chatRoomId, message, role } = opts.input;

      // Mock pushing new chat to db
      const sendChatRequest: SendChatRequest = {
        chatRoomId,
        userId: USER_ID_DUMMY,
        isUser: role === "user" ? true : false,
        message,
        role,
      };

      const lastChat: Chat = {
        ...sendChatRequest,
        id: ulid(),
        sentAt: new Date(),
      };

      // Generate Issue recommendations from user's chat
      const llmRecommendation = await generateRecommendations(lastChat, chatRoomId);
      const llmRecommendationChat = {
        id: ulid(),
        chatRoomId: chatRoomId,
        role: "assistant",
        userId: USER_ID_DUMMY,
        isUser: false,
        message: llmRecommendation.choices[0]?.message.content ?? "",
        sentAt: new Date(),
      } as Chat;

      // Save lastChat and llmRecommendation to DB 
      mockChats.push(lastChat);
      mockChats.push(llmRecommendationChat);

      // Return the LLM recommendation for query based on Gitlab Issues
      return llmRecommendationChat;
    }),
});

const generateRecommendations = async (lastChat: Chat, chatRoomId: string) => {
  // Get data from DB

  // Mock filtering chats by chatRoomId -> convert to Message
  const messages = mockChats
                    .filter((chat) => chat.chatRoomId === chatRoomId)
                    .map((chat) => chatToMessage(chat));

  // const lastMessage = messages[messages.length - 1];
  const lastMessage = chatToMessage(lastChat);
  
  // Embed latest message
  const questionEmbedding = await embedMessage(lastMessage);

  // Retrieve most relevant Gitlab Issue
  const issueEmbeddingRes = `Raw SQL query to use pgvector with query: ${questionEmbedding}`;

  // Convert psql ResultSet rows to string
  // SQL query biasa, order by vec similiarity, retrieve title and url
  // Bisa 0 - 3 references
  const context = issueEmbeddingRes.toString();

  const prompt = generatePrompt(context);

  const llmResponseCompletion = await generateLLMResponse({
    prompt: prompt, 
    messages: messages,
  });

  return llmResponseCompletion;
};

// Helper methods
const embedMessage = async (msg: Message): Promise<Float32Array> => {
  if (msg.content === null) {
    console.error(`embedMessage: msg.content is null`);
    msg.content = "Hello World";
  }
  
  const queryEmbedding = new Float32Array(
    await llm.EmbeddingModel.embedQuery(msg.content)
  );

  return queryEmbedding;
};

const generatePrompt = (context: string): Prompt => {
  return {
    role: "system",
    content: `Your name is Cephalink.
    Cephalink is a brand new, powerful artificial intelligence helper that will help software engineers fix bugs by giving them the power of their Gitlab Issues. 
    Cephalink is a well-behaved and well-mannered individual.
    Cephalink is always friendly, kind, inspiring, and eager to provide vivid and thoughtful responses to the user about their coding queries.
    Relevant information used to answer the user's question about their documents can be found inside of the CONTEXT BLOCK.
    Cephalink will only take into account any information inside the CONTEXT BLOCK below and questions about Cephalink.
    START CONTEXT BLOCK
    ${context}
    END OF CONTEXT BLOCK
    Cephalink will take into account any CONTEXT BLOCK that is provided in a conversation.
    If what the user is asking is not in the CONTEXT BLOCK, Cephalink will say, "I'm sorry, but it seems what you're looking for isn't found in any Issues.".
    If the contents of the CONTEXT BLOCK or context does not provide the answer to the user's question, Cephalink will say, "I'm sorry, but it seems what you're looking for isn't in the repository.".
    Cephalink will not apologize for previous responses, but instead will indicate new information was gained.
    Cephalink will not invent anything that is not drawn directly from the context AKA CONTEXT BLOCK.`,
  } as Prompt;
};

const generateLLMResponse = async ({
  prompt,
  messages,
}: LLMResponseInput): Promise<ChatCompletion> => {
  const promptMessage = prompt as ChatCompletionMessageParam;

  const llmResponse = await llm.openai.chat.completions.create({
    model: "gpt-3.5-turbo",
    temperature: 0.5,
    stream: false,
    messages: [
      promptMessage,
      ...messages?.map((msg) => {
        if (msg.content === null) {
          console.error(`generateLLMResponse: msg.content is null`);
          msg.content = "Hello World";
        }

        const completionMessageParam = {
          role: msg.role as Exclude<ChatCompletionMessageParam["role"], "data">,
          content: msg.content,
        } as ChatCompletionMessageParam;

        return completionMessageParam;
      })
    ]
  });

  return llmResponse;
};