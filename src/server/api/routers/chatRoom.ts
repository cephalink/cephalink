import { z } from "zod";

import { mockChatRooms } from "~/lib/mockData";
import type { ChatRoom } from "~/types/ChatRoom";

import { createTRPCRouter, protectedProcedure, publicProcedure } from "../trpc";

export const chatRoomRouter = createTRPCRouter({
  getChatRooms: publicProcedure.query(({ ctx }) => {
    // Mock userId as 0
    const userId = "0"; // ctx.userAuth?.user.id

    // Mock filtering chat room by id
    const chatRooms: ChatRoom[] = mockChatRooms.filter(
      (room) => room.userId === userId,
    );

    return chatRooms;
  }),
  addNewChat: publicProcedure.mutation(({ ctx }) => {
    // Mock userId as 0
    const USER_ID = "0"; // ctx.userAuth?.user.id

    // Mock adding new chatroom
    const newChatRoom: ChatRoom = {
      id: mockChatRooms.length.toString(),
      lastUpdated: new Date(),
      userId: USER_ID,
    };

    mockChatRooms.push(newChatRoom);

    return newChatRoom;
  }),
  setTitleAndSummary: publicProcedure
    .input(z.object({ id: z.string() }))
    .mutation((opts) => {
      const { id } = opts.input;

      // Mock change title and summary
      const chatRoomIndex = mockChatRooms.findIndex((room) => room.id === id);

      const mockTitle = `Issue #${Math.floor(Math.random() * 1000)}`;
      const mockSummary = `NullPointerError on line 18, the reference 'isActive' is not found.`;

      const chatRoomWithNewTitleAndSummary: ChatRoom = {
        ...mockChatRooms[chatRoomIndex]!,
        title: mockTitle,
        summary: mockSummary,
      };

      mockChatRooms[chatRoomIndex] = chatRoomWithNewTitleAndSummary;

      return chatRoomWithNewTitleAndSummary;
    }),
  setLastUpdate: publicProcedure
    .input(z.object({ id: z.string(), lastUpdated: z.date() }))
    .mutation((opts) => {
      const { id, lastUpdated } = opts.input;

      // Mock change lastUpdated
      const chatRoomIndex = mockChatRooms.findIndex((room) => room.id === id);

      const chatRoomWithNewTitleAndSummary: ChatRoom = {
        ...mockChatRooms[chatRoomIndex]!,
        lastUpdated,
      };

      mockChatRooms[chatRoomIndex] = chatRoomWithNewTitleAndSummary;

      return chatRoomWithNewTitleAndSummary;
    }),
});
