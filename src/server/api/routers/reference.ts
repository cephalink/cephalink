import { z } from "zod";

import { mockReferences } from "~/lib/mockData";
import type { Reference } from "~/types/Reference";

import { createTRPCRouter, publicProcedure } from "../trpc";

export const referenceRouter = createTRPCRouter({
  getChatReferences: publicProcedure
    .input(z.object({ chatId: z.string() }))
    .query((opts) => {
      const { chatId } = opts.input;

      // Mock filtering references
      const references: Reference[] = mockReferences.filter(
        (reference) => reference.chatId === chatId,
      );

      return references;
    }),
  retrieveMoreReferences: publicProcedure
    .input(
      z.object({
        chatId: z.string(),
        chatRoomId: z.string(),
        limit: z.number().default(3),
      }),
    )
    .mutation((opts) => {
      const { chatId, chatRoomId, limit } = opts.input;

      // Mock adding more references
      // Add as much as limit
      const newReferences: Reference[] = [];
      for (let i = 0; i < limit; i++) {
        const newReference: Reference = {
          title: "Reference 2",
          href: "https://www.google.com",
          issueNumber: 2,
          repositoryName: "Repo 2",
          sentAt: new Date(),
          id: mockReferences.length.toString(),
          chatId,
          chatRoomId,
        };

        newReferences.push(newReference);
        mockReferences.push(newReference);
      }

      return newReferences;
    }),
  getChatRoomReferences: publicProcedure
    .input(z.object({ chatRoomId: z.string() }))
    .query((opts) => {
      const { chatRoomId } = opts.input;

      // Mock filtering references
      const references: Reference[] = mockReferences.filter(
        (reference) => reference.chatRoomId === chatRoomId,
      );

      return references;
    }),
});
