import axios from "axios";
import { createTRPCRouter, protectedProcedure } from "../../trpc";
import { TRPCError } from "@trpc/server";
import { GitlabGroup, GitlabProject } from "~/types/resource/dtos";
import {
  addGitlabGroupsFormSchema,
  addGitlabProjectsFormSchema,
  getGroupProjectsFormSchema,
} from "~/types/resource/forms";
import { z } from "zod";

const GITLAB_API_URL = "https://gitlab.com/api/v4";

export const gitlabResourceRouter = createTRPCRouter({
  getGroups: protectedProcedure.query(async ({ ctx }) => {
    const res = await axios.get(GITLAB_API_URL + "/groups", {
      headers: {
        Authorization: `Bearer ${ctx.userAuth.session.accessToken}`,
      },
    });

    if (res.status === 401) {
      throw new TRPCError({
        message: "Your session has expired. Please sign in again.",
        code: "UNAUTHORIZED",
      });
    }

    return res.data as GitlabGroup[];
  }),

  getGroupProjects: protectedProcedure
    .input(getGroupProjectsFormSchema)
    .query(async ({ ctx, input }) => {
      try {
        const res = await axios.get(
          GITLAB_API_URL + "/groups/" + input.groupId + "/projects",
          {
            headers: {
              Authorization: `Bearer ${ctx.userAuth.session.accessToken}`,
            },
          },
        );

        if (res.status === 401) {
          throw new TRPCError({
            message: "Your session has expired. Please sign in again.",
            code: "UNAUTHORIZED",
          });
        }

        return res.data as GitlabProject[];
      } catch (err) {
        console.error(err);
        throw new TRPCError({
          code: "BAD_REQUEST",
          message: "Failed to fetch projects",
        });
      }
    }),

  addGroupsToSystem: protectedProcedure
    .input(addGitlabGroupsFormSchema)
    .mutation(async ({ ctx, input }) => {
      try {
        return ctx.db.$transaction(async (tx) => {
          await tx.gitlabGroup.deleteMany({
            where: {
              userId: ctx.userAuth.user.id,
            },
          });

          return await tx.gitlabGroup.createMany({
            data: input.groups.map(({ id, name }) => ({
              userId: ctx.userAuth.user.id,
              id,
              name,
            })),
          });
        });
      } catch (err) {
        console.error(err);
        throw new TRPCError({
          code: "BAD_REQUEST",
          message: "Failed to add groups to system",
        });
      }
    }),

  addProjectsToSystem: protectedProcedure
    .input(
      z
        .object({
          groupId: z.string(),
        })
        .merge(addGitlabProjectsFormSchema),
    )
    .mutation(async ({ ctx, input }) => {
      try {
        return ctx.db.$transaction(async (tx) => {
          await tx.gitlabProject.deleteMany({
            where: {
              groupId: input.groupId,
              userId: ctx.userAuth.user.id,
            },
          });

          return tx.gitlabProject.createMany({
            data: input.projects.map((project) => ({
              id: project.id,
              groupId: input.groupId,
              userId: ctx.userAuth.user.id,
              name: project.name,
            })),
          });
        });
      } catch (err) {
        console.error(err);
        throw new TRPCError({
          code: "BAD_REQUEST",
          message: "Failed to add projects to system",
        });
      }
    }),
});
