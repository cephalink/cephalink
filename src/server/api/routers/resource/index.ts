import { TRPCError } from "@trpc/server";
import { createTRPCRouter, protectedProcedure } from "../../trpc";
import { gitlabResourceRouter } from "./gitlab";
import { getGroupProjectsFormSchema } from "~/types/resource/forms";

export const resourceRouter = createTRPCRouter({
  gitlab: gitlabResourceRouter,

  getGroups: protectedProcedure.query(async ({ ctx }) => {
    try {
      return ctx.db.gitlabGroup.findMany({
        where: {
          userId: ctx.userAuth.user.id,
        },
      });
    } catch (err) {
      console.error(err);

      throw new TRPCError({
        code: "BAD_REQUEST",
        message: "Failed to fetch groups",
      });
    }
  }),

  getGroupProjects: protectedProcedure
    .input(getGroupProjectsFormSchema)
    .query(async ({ ctx, input }) => {
      const group = await ctx.db.gitlabGroup.findUnique({
        where: {
          id_userId: {
            userId: ctx.userAuth.user.id,
            id: input.groupId,
          },
        },
        include: {
          projects: true,
        },
      });

      if (group === null) {
        throw new TRPCError({
          code: "NOT_FOUND",
          message: "Group not found",
        });
      }

      return group;
    }),
});
