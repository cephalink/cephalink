import { createTRPCRouter, protectedProcedure, publicProcedure } from "../trpc";

export const userRouter = createTRPCRouter({
  getAuthInfo: publicProcedure.query(({ ctx }) => {
    return { userAuth: ctx.userAuth, isAuthenticated: ctx.userAuth !== null };
  }),
});
