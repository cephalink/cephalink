import { PrismaAdapter } from "@lucia-auth/adapter-prisma";
import { GitLab } from "arctic";
import { db } from "./db";
import { env } from "~/env";
import { Lucia, Session, TimeSpan, User } from "lucia";
import { NextApiRequest, NextApiResponse } from "next";
import { parseCookies } from "oslo/cookie";

const dbAdapter = new PrismaAdapter(db.session, db.user);

export const enum AuthProviders {
  GITLAB = "gitlab",
}

export const gitlabAuth = new GitLab(
  env.GITLAB_OAUTH_APPLICATION_ID,
  env.GITLAB_OAUTH_APPLICATION_SECRET,
  env.SERVER_URL + "/api/auth/callback/gitlab",
);

export const auth = new Lucia(dbAdapter, {
  sessionCookie: {
    attributes: {
      secure: env.NODE_ENV === "production",
    },
  },
  getUserAttributes: (user) => ({ ...user }),
  getSessionAttributes: (session) => ({ ...session }),
  sessionExpiresIn: new TimeSpan(3, "h"),
});

export async function handleAuthRequest(
  req: NextApiRequest,
  res: NextApiResponse,
): Promise<{ user: User | null; session: Session | null }> {
  const cookies = parseCookies(req.headers.cookie ?? "");
  const sessionId = cookies.get(auth.sessionCookieName) ?? null;

  if (sessionId === null) {
    return { user: null, session: null };
  }

  const { session, user } = await auth.validateSession(sessionId);
  if (session === null) {
    res.setHeader("Set-Cookie", auth.createBlankSessionCookie().serialize());
  }
  if (session?.fresh) {
    res.setHeader(
      "Set-Cookie",
      auth.createSessionCookie(session.id).serialize(),
    );
  }

  return { user, session };
}

export type Auth = typeof auth;
