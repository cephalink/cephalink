-- CreateTable
CREATE EXTENSION IF NOT EXISTS "vector";

CREATE TABLE "posts" (
    "id" VARCHAR(255) NOT NULL,
    "embedding" vector(3) NOT NULL,

    CONSTRAINT "posts_pkey" PRIMARY KEY ("id")
);
