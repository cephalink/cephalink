-- CreateEnum
CREATE TYPE "user_stage" AS ENUM ('first_sign_in', 'syncing', 'normal');

-- AlterTable
ALTER TABLE "users" ADD COLUMN     "status" "user_stage" NOT NULL DEFAULT 'first_sign_in';
