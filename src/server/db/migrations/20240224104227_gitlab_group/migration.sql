-- CreateTable
CREATE TABLE "gitlab_groups" (
    "id" VARCHAR(50) NOT NULL,
    "user_id" VARCHAR(255) NOT NULL,
    "name" VARCHAR(100) NOT NULL,

    CONSTRAINT "gitlab_groups_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "gitlab_groups" ADD CONSTRAINT "gitlab_groups_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;
