/*
  Warnings:

  - The primary key for the `gitlab_groups` table will be changed. If it partially fails, the table could be left without primary key constraint.

*/
-- AlterTable
ALTER TABLE "gitlab_groups" DROP CONSTRAINT "gitlab_groups_pkey",
ADD CONSTRAINT "gitlab_groups_pkey" PRIMARY KEY ("id", "user_id");

-- CreateTable
CREATE TABLE "gitlab_projects" (
    "id" VARCHAR(50) NOT NULL,
    "group_id" VARCHAR(50) NOT NULL,
    "user_id" VARCHAR(255) NOT NULL,
    "name" VARCHAR(100) NOT NULL,

    CONSTRAINT "gitlab_projects_pkey" PRIMARY KEY ("id","group_id","user_id")
);

-- AddForeignKey
ALTER TABLE "gitlab_projects" ADD CONSTRAINT "gitlab_projects_group_id_user_id_fkey" FOREIGN KEY ("group_id", "user_id") REFERENCES "gitlab_groups"("id", "user_id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "gitlab_projects" ADD CONSTRAINT "gitlab_projects_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;
