import { PrismaClient } from "@prisma/client";
import pgvector from "pgvector";

const db = new PrismaClient();

async function main() {
  await db.$executeRaw`INSERT INTO posts(id, embedding) VALUES ('whew', ${pgvector.toSql([1, 2, 3])}::vector)`;
}

async function runMain() {
  try {
    await main();
    await db.$disconnect();
  } catch (error) {
    console.log(error);
    await db.$disconnect();
    process.exit(1);
  }
}

runMain();
