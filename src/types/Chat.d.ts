export type Chat = {
  id: string;
  chatRoomId: string;
  role: "function" | "system" | "user" | "assistant" | "tool" | string;
  userId: string;
  isUser: boolean;
  message: string;
  sentAt: Date;
};

export type SendChatRequest = Omit<Chat, "id" | "sentAt">;