export type ChatRoom = {
  id: string;
  lastUpdated: Date;
  summary?: string;
  title?: string;
  userId: string;
};