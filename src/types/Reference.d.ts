export type Reference = {
  chatId: string;
  chatRoomId: string;
  href: string;
  id: string;
  issueNumber: number;
  repositoryName: string;
  sentAt: Date;
  title: string;
};
