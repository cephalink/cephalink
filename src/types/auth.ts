import type { User, Session } from "lucia";

export interface AuthenticatedUserProps {
  userAuth: {
    session: Session;
    user: User;
  } | null;
  isAuthenticated: boolean;
}
