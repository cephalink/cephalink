import { lucia } from "lucia";
import type { UserStatus } from "@prisma/client";

declare module "lucia" {
  interface Register {
    Lucia: import("~/server/auth").Auth;
    DatabaseUserAttributes: {
      email: string;
      name: string | null;
      status: UserStatus;
    };
    DatabaseSessionAttributes: {
      userAccountProviderId: string;
      userAccountProviderUserId: string;
      accessToken: string | null;
      refreshToken: string | null;
    };
  }
}
