export interface GitlabGroup {
  id: number;
  name: string;
}

export interface GitlabProject {
  id: number;
  name: string;
}
