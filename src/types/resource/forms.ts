import { z } from "zod";

export const addGitlabGroupsFormSchema = z.object({
  groups: z.array(
    z.object({
      id: z.string(),
      name: z.string(),
    }),
  ),
});

export const getGroupProjectsFormSchema = z.object({
  groupId: z.string(),
});

export const addGitlabProjectsFormSchema = z.object({
  projects: z.array(
    z.object({
      id: z.string(),
      name: z.string(),
    }),
  ),
});
