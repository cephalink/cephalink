import { Chat } from "~/types/Chat";
import { Message } from "ai";

export const chatToMessage = (chat: Chat): Message => {
  return ({
    id: chat.id,
    createdAt: chat.sentAt,
    content: chat.message,
    role: chat.isUser ? "user" : "assistant",
    data: {
      chatRoomId: chat.chatRoomId,
      userId: chat.userId,
    }
  }) as Message;
};