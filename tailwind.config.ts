import type { Config } from "tailwindcss";

const config = {
  darkMode: ["class"],
  content: [
    "./pages/**/*.{ts,tsx}",
    "./components/**/*.{ts,tsx}",
    "./app/**/*.{ts,tsx}",
    "./src/**/*.{ts,tsx}",
  ],
  prefix: "",
  theme: {
    container: {
      center: true,
      padding: "2rem",
      screens: {
        "2xl": "1400px",
      },
    },
    extend: {
      keyframes: {
        "accordion-down": {
          from: { height: "0" },
          to: { height: "var(--radix-accordion-content-height)" },
        },
        "accordion-up": {
          from: { height: "var(--radix-accordion-content-height)" },
          to: { height: "0" },
        },
      },
      animation: {
        "accordion-down": "accordion-down 0.2s ease-out",
        "accordion-up": "accordion-up 0.2s ease-out",
      },
      colors: {
        primary: {
          DEFAULT: "#556AEB",
          100: "#EBEFFF",
          200: "#B9C4FF",
          300: "#8FA0FF",
          400: "#6E82FE",
          500: "#556AEB",
          600: "#354ACB",
          700: "#1D2F99",
          800: "#0C1A66",
          900: "#020A33",
        },
        secondary: {
          DEFAULT: "#FF9500",
          100: "#FFF8EB",
          200: "#FFE3B0",
          300: "#FFCC75",
          400: "#FFB23B",
          500: "#FF9500",
          600: "#CC7C00",
          700: "#996000",
          800: "#664200",
          900: "#332200",
        },
      },
    },
  },
  plugins: [require("tailwindcss-animate")],
} satisfies Config;

export default config;
